<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN APP  CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
     <link rel="stylesheet" type="text/css" href="app-assets/css/pages/chat-application.css">
    <!-- END APP  CSS-->

    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.css">
    <!-- END Page Level CSS-->

    <!-- BEGIN Custom CSS-->
   <link rel="stylesheet" type="text/css" href="app-assets/css/pages/home.css">
    <!-- END Custom CSS-->
	<script src="app-assets/js/jquery.min.js"></script>
	<script src="app-assets/js/bootstrap.js"></script>
</head>

<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar " data-open="click" data-menu="vertical-menu" data-color="">

    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="app-content content main-content">
        <div class="content-wrapper main-wrapper">
            <div class="content-wrapper-before" style="background-image: linear-gradient(to right, #37e5b6, #12916f);background-repeat: repeat-x;">
                  <img class="logo-mob" src="assets/images/logo.png">
            </div>
            <div class="content-body">
                <section class="card">
                    <div id="main-body" class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="app-assets/images/backgrounds/05.jpg">
                                    <div class="navbar-header logo-div" >
                                        <ul class="nav navbar-nav flex-row header-navbar" >
                                            <img src="assets/images/logo pdf.png" style="height: 110px;margin-left: 65px;margin-top: 5px;">
                                        </ul>
                                        <hr>
                                        <!-- <div class="row location-div">
                                            <div class="location-circle" for="toogle" data-role="add-product">
                                              <img src="assets/images/location.png" alt="" style="width: 30px;height: 30px;" />
                                            </div>
                                            <div class="location-text">Kerala</div>
                                        </div> -->
                                         <div class="location-div">
                                         <div class="row">     
                                          <img src="assets/images/loc.png" alt="" style="width: 20px;height: 30px;" />
                                          <span class="distance">10 KM</span>
                                          <span class="change"> Change</span>
                                         </div>
                                         
                                         <div class="location-text">Kerala</div>
                                     </div>
                                    </div>
                                    
                                    <hr>
                                    <div class="main-menu-content" style="background-color: #fff;">
                                        <ul class="navigation navigation-main navigation" id="main-menu-navigation" data-menu="menu-navigation">
                                            <li class=" nav-item">
                                                <a href="#">
                                                          <i class="ft-aperture"></i>
                                                          <span class="menu-title" data-i18n="">All</span>
                                                          <span class="badge badge badge-pill badge-light float-right mr-2">251</span>
                                                        </a>
                                            </li>
                                            <li class=" nav-item">
                                                <a href="#">
                                                          <img src="assets/cat-icons/fruits.png" alt="" class="cat-icon" />
                                                          <span class="menu-title" data-i18n="">Fruits</span>
                                                          <span class="badge badge badge-pill badge-light float-right mr-2">20</span>
                                                        </a>
                                            </li>
                                            <li class=" nav-item">
                                                <a href="#">
                                                          <img src="assets/cat-icons/vegitables.png" alt="" class="cat-icon" />
                                                          <span class="menu-title" data-i18n="">Vegitables</span>
                                                          <span class="badge badge badge-pill badge-light float-right mr-2">51</span>
                                                        </a>
                                            </li>
                                            <li class=" nav-item">
                                                <a href="#">
                                                          <img src="assets/cat-icons/wine.png" alt="" class="cat-icon" />
                                                          <span class="menu-title" data-i18n="">Wine</span>
                                                          <span class="badge badge badge-pill badge-light float-right mr-2">10</span>
                                                        </a>
                                            </li>
                                            <li class=" nav-item">
                                                <a href="#">
                                                         <img src="assets/cat-icons/cropsw.png" alt="" class="cat-icon" />
                                                          <span class="menu-title" data-i18n="">Cropsw</span>
                                                        </a>
                                            </li>
                                            <li class=" nav-item">
                                                <a href="#">
                                                         <img src="assets/cat-icons/freefood.png" alt="" class="cat-icon" />
                                                          <span class="menu-title" data-i18n="">Freefood</span>
                                                        </a>
                                            </li>
                                            <li class=" nav-item">
                                                <a href="#">
                                                         <img src="assets/cat-icons/cakes.png" alt="" class="cat-icon" />
                                                          <span class="menu-title" data-i18n="">Cakes</span>
                                                        </a>
                                            </li>
                                            <li class=" nav-item">
                                                <a href="#">
                                                         <img src="assets/cat-icons/homelymeals.png" alt="" class="cat-icon" />
                                                          <span class="menu-title" data-i18n="">Homely Meals</span>
                                                        </a>
                                            </li>
                                            <li class=" nav-item">
                                                <a href="#">
                                                         <img src="assets/cat-icons/milkproducts.png" alt="" class="cat-icon" />
                                                          <span class="menu-title" data-i18n="">Milk Products</span>
                                                        </a>
                                            </li>
                                            <li class=" nav-item">
                                                <a href="#">
                                                         <img src="assets/cat-icons/oil.png" alt="" class="cat-icon" />
                                                          <span class="menu-title" data-i18n="">Oil</span>
                                                        </a>
                                            </li>
                                            <li class=" nav-item">
                                                <a href="#">
                                                         <img src="assets/cat-icons/smarthomeworks.png" alt="" class="cat-icon" />
                                                          <span class="menu-title" data-i18n="">Smart Home Works</span>
                                                        </a>
                                            </li>
                                            <li class=" nav-item">
                                                <a href="#">
                                                         <img src="assets/cat-icons/spices.png" alt="" class="cat-icon" />
                                                          <span class="menu-title" data-i18n="">spices</span>
                                                        </a>
                                            </li>
                                            <li class=" nav-item">
                                                <a href="#">
                                                         <img src="assets/cat-icons/sweetsnacks.png" alt="" class="cat-icon" />
                                                          <span class="menu-title" data-i18n="">Sweet and Snacks</span>
                                                        </a>
                                            </li>

                                        </ul>
                                    </div>
                                    <div class="navigation-background"></div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow navbar-semi-light navigation-inner col-xs-12">
                                    <div class="navbar-wrapper">
                                        <div class="navbar-container">
                                            <div class="collapse navbar-collapse show" id="navbar-mobile">
                                                <ul class="nav navbar-nav mr-auto float-left search-menu">
                                                   <li>
                                                        <div class="toggle-wrapper">
                                                            <div class="toggle_radio">
                                                                <input type="radio" class="toggle_option" id="first_toggle" name="toggle_option">
                                                                <input type="radio" checked class="toggle_option" id="second_toggle" name="toggle_option">
                                                                <input type="radio" class="toggle_option" id="third_toggle" name="toggle_option">
                                                                <label for="first_toggle">
                                                                          <p>Buy</p>
                                                                        </label>
                                                                <label for="second_toggle">
                                                                          <p>All</p>
                                                                        </label>
                                                                <label for="third_toggle">
                                                                          <p>Wishlist</p>
                                                                        </label>
                                                                <div class="toggle_option_slider"></div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="nav-item d-none d-md-block" data-toggle="tooltip" title="Add an Item"  >
                                                        <input class="hidden-trigger" id="toogle" type="checkbox">
                                                        <label class="circle add-icon" for="toogle" data-role="add-product"  data-toggle="modal" data-target="#addModal">
                                                        <img src="assets/images/add-icon.png" alt="" />
                                                      </label>
                                                    </li>

                                                    <li class="nav-item d-none d-md-block" data-toggle="tooltip" title="Add a Wish Item" >
                                                        <input class="hidden-trigger" id="toogle" type="checkbox">
                                                        <label class="circle wish-icon" for="toogle" data-role="add-wish" data-toggle="modal" data-target="#wishModal">
                                                            <img src="assets/images/wish.png" alt="" />
                                                          </label>
                                                    </li>
                                                    
                                                    <li class="nav-item mobile-menu d-md-none mr-auto search-box hidden-tablet hidden-desktop hidden-lg">
                                                        <a class="nav-link nav-menu-main menu-toggle " href="#">
                                                              <i class="ft-menu font-large-1"></i>
                                                            </a>
                                                    </li>                                                  

                                                </ul>
                                                <ul class="nav navbar-nav float-right notification-ui">
                                                  <li class="dropdown nav-item mega-dropdown d-none d-md-block">
                                                        <form>
                                                            <div class="card-body chat-fixed-search" style="width: 284px;">
                                  <fieldset class="form-group position-relative has-icon-left m-0  w-75 display-inline">
                                      <input type="text" class="form-control round" id="searchUser" placeholder="Search here">
                                     <!-- <div class="form-control-position">
                                          <i class="ft-search"></i>
                                      </div>  -->     
                                  </fieldset>  
                              </div>
                                                        </form>
                                                    </li>
                                                    <li class="dropdown nav-item mega-dropdown d-none d-md-block"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="la la-sliders mr-1 filter-btn"></i></a>
                                    <ul class="mega-dropdown-menu dropdown-menu row">
                                      <li class="col-md-3">
                                        <h6 class="dropdown-menu-header text-uppercase mb-1"><i class="ft-link"></i>Type of Bussiness</h6>
                                            <select id="companyinput5" name="interested" class="form-control">
                                                <option value="none" selected="" disabled="">Local Shop</option>
                                                <option value="design">US</option>
                                            </select>
                                      </li>
                                      <li class="col-md-3">
                                        <h6 class="dropdown-menu-header text-uppercase mb-1"><i class="ft-star"></i> Delivery Available</h6>
                                        <select id="companyinput5" name="interested" class="form-control">
                                                <option value="none" selected="" disabled="">Yes</option>
                                                <option value="design">No</option>
                                            </select>
                                        
                                      </li>
                                      <li class="col-md-4">
                                        <h6 class="dropdown-menu-header text-uppercase"><i class="ft-layers"></i> Price Range</h6>
                                        <form class="range-field">
                                          <input type="range" min="0" max="100" />
                                        </form>
                                        
                                      </li>
                                      <li class="col-md-2">
                                        <h6 class="dropdown-menu-header text-uppercase"><i class="ft-layers"></i> Search</h6>
                                        <fieldset>
                                                    <input type="checkbox" id="input-5">
                                                    <label for="input-5">Everywhere</label>
                                                </fieldset>
                                      </li>
                                    </ul>
                                  </li>
                                                    <li class="dropdown dropdown-notification nav-item">
                                                        <a class="nav-link nav-link-label" href="#" data-toggle="dropdown">
                                                                        <i class="ficon ft-bell bell-shake" id="notification-navbar-link"></i>
                                                                        <span class="badge badge-pill badge-sm badge-danger badge-default badge-up badge-glow">5</span>
                                                                      </a>
                                                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                                            <div class="arrow_box_right">
                                                                <li class="dropdown-menu-header">
                                                                    <h6 class="dropdown-header m-0">
                                                                        <span class="grey darken-2">Notifications</span>
                                                                    </h6>
                                                                </li>
                                                                <li class="scrollable-container media-list w-100">
                                                                    <a href="javascript:void(0)">
                                                                        <div class="media">
                                                                            <div class="media-left align-self-center">
                                                                                <i class="ft-share info font-medium-4 mt-2"></i>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <h6 class="media-heading info">New Order Received</h6>
                                                                                <p class="notification-text font-small-3 text-muted text-bold-600">Lorem ipsum dolor sit amet!</p>
                                                                                <small>
                                                                                    <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">3:30 PM</time>
                                                                                  </small>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                    <a href="javascript:void(0)">
                                                                        <div class="media">
                                                                            <div class="media-left align-self-center">
                                                                                <i class="ft-save font-medium-4 mt-2 warning"></i>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <h6 class="media-heading warning">New User Registered</h6>
                                                                                <p class="notification-text font-small-3 text-muted text-bold-600">Aliquam tincidunt mauris eu risus.</p>
                                                                                <small>
                                                                                    <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">10:05 AM</time>
                                                                                  </small>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                    <a href="javascript:void(0)">
                                                                        <div class="media">
                                                                            <div class="media-left align-self-center">
                                                                                <i class="ft-repeat font-medium-4 mt-2 danger"></i>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <h6 class="media-heading danger">New Purchase</h6>
                                                                                <p class="notification-text font-small-3 text-muted text-bold-600">Lorem ipsum dolor sit ametest?</p>
                                                                                <small>
                                                                                    <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Yesterday</time>
                                                                                  </small>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                    <a href="javascript:void(0)">
                                                                        <div class="media">
                                                                            <div class="media-left align-self-center">
                                                                                <i class="ft-shopping-cart font-medium-4 mt-2 primary"></i>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <h6 class="media-heading primary">New Item In Your Cart</h6>
                                                                                <small>
                                                                                    <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Last week</time>
                                                                                  </small>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                    <a href="javascript:void(0)">
                                                                        <div class="media">
                                                                            <div class="media-left align-self-center">
                                                                                <i class="ft-heart font-medium-4 mt-2 info"></i>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <h6 class="media-heading info">New Sale</h6>
                                                                                <small>
                                                                                    <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Last month</time>
                                                                                  </small>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li class="dropdown-menu-footer">
                                                                    <a class="dropdown-item info text-right pr-1" href="javascript:void(0)">Read all</a>
                                                                </li>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown dropdown-notification nav-item">
                                                        <a class="nav-link nav-link-label" href="#" data-toggle="dropdown">
                                                                        <i class="ficon ft-mail"></i>
                                                                      </a>
                                                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                                            <div class="arrow_box_right">
                                                                <li class="dropdown-menu-header">
                                                                    <h6 class="dropdown-header m-0">
                                                                        <span class="grey darken-2">Messages</span>
                                                                    </h6>
                                                                </li>
                                                                <li class="scrollable-container media-list w-100">
                                                                    <a href="javascript:void(0)">
                                                                        <div class="media">
                                                                            <div class="media-left">
                                                                                <span class="avatar avatar-sm rounded-circle">
                                                                                    <img src="app-assets/images/portrait/small/avatar-s-6.png" alt="avatar">
                                                                                    </span>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <h6 class="media-heading text-bold-700">Sarah Montery

                                                                                    <i class="ft-circle font-small-2 success float-right"></i>
                                                                                </h6>
                                                                                <p class="notification-text font-small-3 text-muted text-bold-600">Everything looks good. I will provide...</p>
                                                                                <small>
                                                                                      <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">3:55 PM</time>
                                                                                    </small>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                    <a href="javascript:void(0)">
                                                                        <div class="media">
                                                                            <div class="media-left">
                                                                                <span class="avatar avatar-sm rounded-circle">
                                                                                      <span class="media-object rounded-circle text-circle bg-warning">E</span>
                                                                                </span>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <h6 class="media-heading text-bold-700">Eliza Elliot

                                                                                    <i class="ft-circle font-small-2 danger float-right"></i>
                                                                                </h6>
                                                                                <p class="notification-text font-small-3 text-muted text-bold-600">Okay. here is some more details...</p>
                                                                                <small>
                                                                                      <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">2:10 AM</time>
                                                                                    </small>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                    <a href="javascript:void(0)">
                                                                        <div class="media">
                                                                            <div class="media-left">
                                                                                <span class="avatar avatar-sm rounded-circle">
                                                                                      <img src="app-assets/images/portrait/small/avatar-s-3.png" alt="avatar">
                                                                                      </span>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <h6 class="media-heading text-bold-700">Kelly Reyes

                                                                                    <i class="ft-circle font-small-2 warning float-right"></i>
                                                                                </h6>
                                                                                <p class="notification-text font-small-3 text-muted text-bold-600">Check once and let me know if you...</p>
                                                                                <small>
                                                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Yesterday</time>
                                                                                      </small>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                    <a href="javascript:void(0)">
                                                                        <div class="media">
                                                                            <div class="media-left">
                                                                                <span class="avatar avatar-sm rounded-circle">
                                                                                        <img src="app-assets/images/portrait/small/avatar-s-19.png" alt="avatar">
                                                                                        </span>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <h6 class="media-heading text-bold-700">Tonny Deep

                                                                                    <i class="ft-circle font-small-2 danger float-right"></i>
                                                                                </h6>
                                                                                <p class="notification-text font-small-3 text-muted text-bold-600">We will start new project development...</p>
                                                                                <small>
                                                                                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Friday</time>
                                                                                        </small>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li class="dropdown-menu-footer">
                                                                    <a class="dropdown-item text-right info pr-1" href="javascript:void(0)">Read all</a>
                                                                </li>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown dropdown-user nav-item" >
                                                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown" data-role="view-login">
                                                                              <span class="avatar avatar-online">
                                                                                <img src="assets/images/default-user.png" alt="avatar">
                                                                                </span>
                                                                              </a>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <div class="arrow_box_right">
                                                                <a class="dropdown-item" href="#">
                                                                                    <span class="avatar avatar-online">
                                                                                      <img src="app-assets/images/portrait/small/avatar-s-19.png" alt="avatar">
                                                                                        <span class="user-name text-bold-700 ml-1">John Doe</span>
                                                                                      </span>
                                                                                    </a>
                                                                <div class="dropdown-divider"></div>
                                                                <a class="dropdown-item" data-toggle="modal" data-target="#signupModal">
                                                                                      <i class="ft-user"></i> Edit Profile
                                                                                    
                                                                                    </a>
                                                                <a class="dropdown-item" href="email-application.html">
                                                                                      <i class="ft-mail"></i> My Inbox
                                                                                    
                                                                                    </a>
                                                                <a class="dropdown-item" href="project-summary.html">
                                                                                      <i class="ft-check-square"></i> Task
                                                                                    
                                                                                    </a>
                                                                <a class="dropdown-item" href="chat-application.html">
                                                                                      <i class="ft-message-square"></i> Chats
                                                                                    
                                                                                    </a>
                                                                <div class="dropdown-divider"></div>
                                                                <a class="dropdown-item" href="login.html">
                                                                                      <i class="ft-power"></i> Logout
                                                                                    
                                                                                    </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="dropdown nav-item mega-dropdown d-none d-md-block" style="margin-left: -22px;margin-right:-22px;">
                                                        <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                                                                                  <i class="la la-ellipsis-v"></i>
                                                                                </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </nav>
                                <div class="product-body ps-container ps-theme-dark ps-active-y" data-ps-id="639e1184-607f-9a96-02d2-4eaaf8ef1868">

                                    <div class="row product-row">
                                        <div class="col-xs-12 col-sm-12 col-lg-6 product-item ">
                                            <div class="card mb-1 border-info  border-lighten-4">
                                                <div class="card-content row">
                                                    <div class="col-xs-12 col-sm-5 ">
                                                        <div data-ride="carousel" class="carousel slide" id="carousel-area">
                                                            <img class="d-block w-100 product-image" src="assets/images/sample-products/mango.jpg">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-7 product-body-right">

                                                        <div class="card-body ">
                                                            <h4 class="card-title" data-role="view-product" data-toggle="modal" data-target="#productModal">Mango
                                                                <div class="pull-right badge border-info info badge-border">10 Km away</div>
                                                            </h4>
                                                            <!-- <span class="float-left">2 days ago</span> -->
                                                            <span class="product-place"><i class="ft-map-pin"></i> Thissur</span>
                                                            <span class="product-date"> <i class="ft-check-circle"></i> 2 Days Ago</span><br>
                                                            <span class="product-price pull-right"><i class="la la-rupee"></i>200 <span class="product-quantity">(4 Kg)</span></span>


                                                        </div>
                                                        <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                                                            <span class="tags pull-left">
                                                    <span class="badge badge-pill badge-warning"><i class="ft-eye"></i> 12</span></span>

                                                            <span class="tags pull-right">     
                                                  <div class="badge success round badge-border">
                                                    <i class="font-medium-5 icon-line-height ft-user" style="font-size: 2.2rem;"></i> 
                                                  </div>      
                                                  <div class="badge info round badge-border" data-role=view-chat data-toggle="modal" data-target="#chatModal">
                                                        <i class="font-medium-5 icon-line-height ft-message-square" style="font-size: 2.2rem;"></i> 
                                                      </div>                                      
                                                      <div class="badge danger round badge-border">
                                                        <i class="font-medium-5 icon-line-height la la-heart" style="color: #FA626B;font-size: 2.2rem;"></i>
                                                      </div>
                                                  </span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-lg-6  product-item ">
                                            <div class="card mb-1 border-info  border-lighten-4">
                                                <div class="card-content row">
                                                    <div class="col-xs-12 col-sm-5 ">
                                                        <div data-ride="carousel" class="carousel slide" id="carousel-area">
                                                            <img class="d-block w-100 product-image" src="assets/images/sample-products/jathi.jpg">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-7 product-body-right">

                                                        <div class="card-body ">
                                                            <h4 class="card-title">Nutmeg
                                                                <div class="pull-right badge border-info info badge-border">10 Km away</div>
                                                            </h4>
                                                            <!-- <span class="float-left">2 days ago</span> -->
                                                            <span class="product-place"><i class="ft-map-pin"></i> Thissur</span>
                                                            <span class="product-date"> <i class="ft-check-circle"></i> 2 Days Ago</span><br>
                                                            <span class="product-price pull-right"><i class="la la-rupee"></i>200 <span class="product-quantity">(4 Kg)</span></span>


                                                        </div>
                                                        <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                                                            <span class="tags pull-left">
                                                <span class="badge badge-pill badge-warning"><i class="ft-eye"></i> 12</span></span>

                                                            <span class="tags pull-right"> 
                                                  <div class="badge success round badge-border">
                                                    <i class="font-medium-5 icon-line-height ft-user" style="font-size: 2.2rem;"></i> 
                                                  </div>          
                                                  <div class="badge info round badge-border">
                                                    <i class="font-medium-5 icon-line-height ft-message-square" style="font-size: 2.2rem;"></i> 
                                                  </div>                                      
                                                  <div class="badge danger round badge-border">
                                                    <i class="font-medium-5 icon-line-height ft-heart" style="font-size: 2.2rem;"></i> 
                                                  </div>
                                              </span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row product-row">
                                        <div class="col-xs-12 col-sm-12 col-lg-6  product-item ">
                                            <div class="card mb-1  border-top-lighten-4 border-top-5">
                                                <div class="card-content row">
                                                    <div class="col-xs-12 col-sm-5 ">
                                                        <div data-ride="carousel" class="carousel slide" id="carousel-area">
                                                            <img class="d-block w-100 product-image" src="assets/images/sample-products/roseapple.jpg">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-7 product-body-right">

                                                        <div class="card-body ">
                                                            <h4 class="card-title">Rose Apple Wine
                                                                <div class="pull-right badge border-info info badge-border">10 Km away</div>
                                                            </h4>
                                                            <!-- <span class="float-left">2 days ago</span> -->
                                                            <span class="product-place"><i class="ft-map-pin"></i> Thissur</span>
                                                            <span class="product-date"> <i class="ft-check-circle"></i> 2 Days Ago</span><br>
                                                            <span class="product-price pull-right"><i class="la la-rupee"></i>200 <span class="product-quantity">(4 Kg)</span></span>


                                                        </div>
                                                        <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                                                            <span class="tags pull-left">
                                                    <span class="badge badge-pill badge-warning"><i class="ft-eye"></i> 12</span></span>

                                                            <span class="tags pull-right">  
                                                      <div class="badge success round badge-border">
                                                        <i class="font-medium-5 icon-line-height ft-user" style="font-size: 2.2rem;"></i> 
                                                      </div>         
                                                      <div class="badge info round badge-border">
                                                        <i class="font-medium-5 icon-line-height ft-message-square" style="font-size: 2.2rem;"></i> 
                                                      </div>                                      
                                                      <div class="badge danger round badge-border">
                                                        <i class="font-medium-5 icon-line-height la la-heart" style="color: #FA626B;font-size: 2.2rem;"></i>
                                                      </div>
                                                  </span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-lg-6  product-item ">
                                            <div class="card wish-border">
                                                <div class="card-content row">
                                                    <div class="col-xs-12 col-sm-5 ">
                                                        <div data-ride="carousel" class="carousel slide" id="carousel-area">
                                                            <img class="d-block w-100 product-image" src="assets/images/sample-products/mango.jpg">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-7 product-body-right">

                                                        <div class="card-body ">
                                                            <h4 class="card-title">Mango
                                                                <div class="pull-right badge border-purple purple round badge-border"><img src="assets/images/wish-icon.png" alt="" style="height:30px;width: 30px;padding:1px;" /></div>
                                                            </h4>
                                                            <!-- <span class="float-left">2 days ago</span> -->
                                                            <span class="product-place"><i class="ft-map-pin"></i> Thissur</span>
                                                            <span class="product-date"> <i class="ft-check-circle"></i> 2 Days Ago</span><br>
                                                            <span class="product-price pull-right"><i class="la la-rupee"></i>200 <span class="product-quantity">(4 Kg)</span></span>


                                                        </div>
                                                        <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                                                            <span class="tags pull-left">
                                                <span class="badge badge-pill badge-warning"><i class="ft-eye"></i> 12</span></span>

                                                            <span class="tags pull-right">          
                                                  <div class="badge success round badge-border">
                                                    <i class="font-medium-5 icon-line-height ft-user" style="font-size: 2.2rem;"></i> 
                                                  </div> 
                                                  <div class="badge info round badge-border">
                                                    <i class="font-medium-5 icon-line-height ft-message-square" style="font-size: 2.2rem;"></i> 
                                                  </div>                                      
                                                  <div class="badge danger round badge-border">
                                                    <i class="font-medium-5 icon-line-height ft-heart" style="font-size: 2.2rem;"></i> 
                                                  </div>
                                              </span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row product-row">
                                        <div class="col-xs-12 col-sm-12 col-lg-6  product-item wishlist-row">
                                            <div class="card mb-1 ">
                                                <div class="card-content row">
                                                    <div class="col-xs-12 col-sm-5 ">
                                                        <div data-ride="carousel" class="carousel slide" id="carousel-area">
                                                            <img class="d-block w-100 product-image" src="assets/images/sample-products/banana.jpg">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-7 product-body-right">

                                                        <div class="card-body ">


                                                            <h4 class="card-title">Banana
                                                                <div class="pull-right badge border-purple purple round badge-border"><img src="assets/images/wish-icon.png" alt="" style="height:30px;width: 30px;padding:1px;"/></div>
                                                            </h4>
                                                            <!-- <span class="float-left">2 days ago</span> -->
                                                            <span class="product-place"><i class="ft-map-pin"></i> Thissur</span>
                                                            <span class="product-date"> <i class="ft-check-circle"></i> 2 Days Ago</span><br>
                                                            <span class="product-price pull-right"><i class="la la-rupee"></i>200 <span class="product-quantity">(4 Kg)</span></span>


                                                        </div>
                                                        <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                                                            <span class="tags pull-left">
                                                    <span class="badge badge-pill badge-warning"><i class="ft-eye"></i> 12</span></span>

                                                            <span class="tags pull-right">          
                                                  <div class="badge info round badge-border">
                                                        <i class="font-medium-5 icon-line-height ft-message-square" style="font-size: 2.2rem;"></i> 
                                                      </div>                                      
                                                      <div class="badge danger round badge-border">
                                                        <i class="font-medium-5 icon-line-height la la-heart" style="color: #FA626B;font-size: 2.2rem;"></i>
                                                      </div>
                                                  </span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-lg-6  product-item ">
                                            <div class="card mb-1 border-info  border-lighten-4">
                                                <div class="card-content row">
                                                    <div class="col-xs-12 col-sm-5 ">
                                                        <div data-ride="carousel" class="carousel slide" id="carousel-area">
                                                            <img class="d-block w-100 product-image" src="assets/images/sample-products/mango1.jpg">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-7 product-body-right">

                                                        <div class="card-body ">
                                                            <h4 class="card-title">Mango
                                                                <div class="pull-right badge border-info info badge-border">10 Km away</div>
                                                            </h4>
                                                            <!-- <span class="float-left">2 days ago</span> -->
                                                            <span class="product-place"><i class="ft-map-pin"></i> Thissur</span>
                                                            <span class="product-date"> <i class="ft-check-circle"></i> 2 Days Ago</span><br>
                                                            <span class="product-price pull-right"><i class="la la-rupee"></i>200 <span class="product-quantity">(4 Kg)</span></span>


                                                        </div>
                                                        <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                                                            <span class="pull-left tags">
                                                <span class="badge badge-pill badge-warning"><i class="ft-eye"></i> 12</span></span>

                                                            <span class="tags pull-right">          
                                              <div class="badge info round badge-border">
                                                    <i class="font-medium-5 icon-line-height ft-message-square" style="font-size: 2.2rem;"></i> 
                                                  </div>                                      
                                                  <div class="badge danger round badge-border">
                                                    <i class="font-medium-5 icon-line-height ft-heart" style="font-size: 2.2rem;"></i> 
                                                  </div>
                                              </span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row ad-row">
                                        <div class="col-xs-12 col-sm-12  product-item ">
                                            <img class="responsive" src="assets/images/ads.png">
                                        </div>
                                    </div>
                                    <div class="row product-row">
                                        <div class="col-xs-12 col-sm-12 col-lg-6  product-item ">
                                            <div class="card mb-1 border-info  border-lighten-4">
                                                <div class="card-content row">
                                                    <div class="col-xs-12 col-sm-5 ">
                                                        <div data-ride="carousel" class="carousel slide" id="carousel-area">
                                                            <img class="d-block w-100 product-image" src="assets/images/sample-products/mango.jpg">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-7 product-body-right">

                                                        <div class="card-body ">
                                                            <h4 class="card-title">Mango
                                                                <div class="pull-right badge border-info info badge-border">10 Km away</div>
                                                            </h4>
                                                            <!-- <span class="float-left">2 days ago</span> -->
                                                            <span class="product-place"><i class="ft-map-pin"></i> Thissur</span>
                                                            <span class="product-date"> <i class="ft-check-circle"></i> 2 Days Ago</span><br>
                                                            <span class="product-price  pull-right"><i class="la la-rupee"></i>200 <span class="product-quantity">(4 Kg)</span></span>


                                                        </div>
                                                        <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                                                            <span class="pull-left tags">
                                                    <span class="badge badge-pill badge-warning"><i class="ft-eye"></i> 12</span></span>

                                                            <span class="tags pull-right">          
                                                  <div class="badge info round badge-border">
                                                        <i class="font-medium-5 icon-line-height ft-message-square" style="font-size: 2.2rem;"></i> 
                                                      </div>                                      
                                                      <div class="badge danger round badge-border">
                                                        <i class="font-medium-5 icon-line-height la la-heart" style="color: #FA626B;font-size: 2.2rem;"></i>
                                                      </div>
                                                  </span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-lg-6  product-item ">
                                            <div class="card card mb-1 border-info  border-lighten-4">
                                                <div class="card-content row">
                                                    <div class="col-xs-12 col-sm-5 ">
                                                        <div data-ride="carousel" class="carousel slide" id="carousel-area">
                                                            <img class="d-block w-100 product-image" src="assets/images/sample-products/mango.jpg">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-7 product-body-right">

                                                        <div class="card-body ">
                                                            <h4 class="card-title">Mango
                                                                <div class="pull-right badge border-info info badge-border">10 Km away</div>
                                                            </h4>
                                                            <!-- <span class="float-left">2 days ago</span> -->
                                                            <span class="product-place"><i class="ft-map-pin"></i> Thissur</span>
                                                            <span class="product-date"> <i class="ft-check-circle"></i> 2 Days Ago</span><br>
                                                            <span class="product-price pull-right"><i class="la la-rupee"></i>200 <span class="product-quantity">(4 Kg)</span></span>


                                                        </div>
                                                        <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                                                            <span class="pull-left tags">
                                                <span class="badge badge-pill badge-warning"><i class="ft-eye"></i> 12</span></span>

                                                            <span class="tags pull-right">          
                                              <div class="badge info round badge-border">
                                                    <i class="font-medium-5 icon-line-height ft-message-square" style="font-size: 2.2rem;"></i> 
                                                  </div>                                      
                                                  <div class="badge danger round badge-border">
                                                    <i class="font-medium-5 icon-line-height ft-heart" style="font-size: 2.2rem;"></i> 
                                                  </div>
                                              </span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- Menu Invoice Company Details -->
                            <!--/ Invoice Footer -->
                        </div>
                </section>
                </div>
            </div>
        </div>
        <?php include 'modals/add_item.php';?>
        <?php include 'modals/add_wish.php';?>
        <?php include 'modals/chat.php';?>
        <?php include 'modals/view_product.php';?>
        <?php include 'modals/signup.php';?>
        

        <!-- ////////////////////////////////////////////////////////////////////////////-->
        <!-- BEGIN VENDOR JS-->
        <script src="app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN APP  JS-->
        <script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
        <script src="app-assets/js/core/app.js" type="text/javascript"></script>
        <!-- END APP  JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <!-- END PAGE LEVEL JS-->
        <script type="text/javascript">
          $(".product-body").perfectScrollbar({
            suppressScrollX: true,
            scrollXMarginOffset  :100,
          }); 
          $(".list-group").perfectScrollbar({
            suppressScrollX: true,
            scrollXMarginOffset  :100,
          }); 
        </script>
		<style>
		@media (max-width: 340px) and (min-width: 321px)
{
	.notification-ui {
    margin-top: -160px !important;
    margin-right: -94px !important;
}
.mobile-menu.hidden-lg {
    position: absolute;
    top: 0;
    right: -16px !important;
}
}
.dropdown .dropdown-menu .dropdown-item {
    width: 100%;
    padding: 10px 20px;
    display: inline-block !important;
}
		</style>
</body>

</html>