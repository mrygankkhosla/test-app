<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="modal" id="loginModal" role="dialog">
<div class="modal-dialog modal-dialog-centered modal-m " role="document">
	<div class="modal-content border-info" >
		<div class="modal-header bg- white">
		<h4 class="modal-title white" id="myModalLabel11">Basic Modal</h4>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		</div>
		<div class="modal-body" >
		 <div class="col-12 d-flex align-items-center justify-content-center">
        <div class="col-md-12 col-10 box-shadow-2 p-0">
            <div class=" border-grey border-lighten-3 px-1 py-1 m-0">
                <div class="card-header border-0">
                    <div class="text-center mb-1">
                            <img src="assets/images/logo.png" alt="branding logo">
                    </div>
                    <div class="font-large-1  text-center">                       
                        Login
                    </div>
                </div>
                <div class="card-content">
                   
                    <div class="card-body">
                        <form class="form-horizontal needs-validation" id="login-frm" novalidate>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" class="form-control round was-validated" id="identity" placeholder="Email or Phone" name="identity" required>
                                <div class="form-control-position">
                                    <i class="ft-user"></i>
                                </div>
                            </fieldset>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input id="password" type="password" class="form-control round" id="user-password" placeholder="Enter Password" required>
                                <div class="form-control-position">
                                    <i class="ft-lock"></i>
                                </div>
                            </fieldset>
                           <div class="g-recaptcha" data-sitekey="6LePYpMUAAAAAMr37s_FNO8HbBS2qx0aJxO3nZyH" data-callback="enableBtn"></div>
                            <div class="form-group row">
                                <div class="col-md-6 col-12 text-center text-sm-left">
                                   
                                </div>
                                <div style="margin-top: : 15px;" class="col-md-6 col-12 float-sm-left text-center text-sm-right"><a href="l" class="card-link">Forgot Password?</a></div>
                            </div>                           
                            <div class="form-group text-center">
                                <button type="submit" class="btn round btn-block btn-glow btn-bg-gradient-x-purple-blue col-12 mr-1 mb-1" name="login" id="login">Login</button>    
                            </div>
                           
                        </form>
                    </div>
                                        
                    <p class="card-subtitle text-muted text-right font-small-3 mx-2 my-1"><span>Don't have an account ? <a href="register.html" class="card-link">Sign Up</a></span></p>                    
                </div>
            </div>
        </div>
    </div>
		</div>
		<!-- <div class="modal-footer">
		<button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-info">Save</button>
		</div> -->
	</div>
</div>
</div>
	