<div class="modal" id="productModal" role="dialog">
<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
	<div class="modal-content border-info">
		<div class="modal-header bg- white">
		<h4 class="modal-title white" id="myModalLabel11">Basic Modal</h4>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		</div>
		<div class="modal-body">
		<div class="row col-md-12">
			<div class="col-md-5">                
				<div id="carousel-area" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-area" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-area" data-slide-to="1"></li>
                        <li data-target="#carousel-area" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="assets/images/sample-products/mango.jpg" class="d-block w-100" alt="First slide">
                        </div>
                        <div class="item">
                            <img src="assets/images/sample-products/mango.jpg" class="d-block w-100" alt="Second slide">
                        </div>
                        <div class="item">
                            <img src="assets/images/sample-products/jathi.jpg" class="d-block w-100" alt="Third slide">
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carousel-area"  data-slide="prev">
                            <span class="la la-angle-left icon-prev" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                    <a class="right carousel-control" href="#carousel-area"  data-slide="next">
                            <span class="la la-angle-right icon-next" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                </div>	
                 <span class="product-price-quick float"><i class="la la-rupee"></i>200 <span class="product-quantity">(4 Kg)</span></span>
			</div>
			<div class="col-md-7">
				<h2 class="product-name">Mango  <div class="badge danger secondary round badge-border">
                    <i class="font-medium-5 icon-line-height ft-heart" style="font-size: 3.2rem;"></i> 
                                      </div>
                    <img src="assets/images/home.png" style="width: 65px;float: right;">
                    </h2>
				
                <div class="row"> 
               
				<span class="product-place-quick"><i class="ft-map-pin"></i> Thissur</span>
                <span class="distance-quick badge border-success info badge-border">10 Km away</span>
                <span class="product-date-quick"> <i class="ft-check-circle"></i> 2 Days Ago</span>
                <span class="product-id-quick" style="margin-left: 19px;"><i class="ft-eye"></i> 236</span>
                <span class="product-id-quick"></i>ID : 48516</span>        
            <!--     <span><i class="la la-close delivery-icon"></i>Home Delivery</span>  -->  
                 </div>
           
               
               <hr>
                <div class="row"> 
                	<div class="col-md-12">
                		<h5>Description</h5>
						<p>Oat cake ice cream candy chocolate cake chocolate cake cotton candy dragée apple pie. Brownie carrot cake candy canes bonbon fruitcake topping halvah. Cake sweet roll cake cheesecake cookie chocolate cake liquorice. Apple pie sugar plum powder donut soufflé.</p>
                	</div>
                	<!-- <div class="col-md-2 price-div">
                         <span class="product-price-quick float"><i class="la la-rupee"></i>200 <span class="product-quantity">(4 Kg)</span></span>
                	</div> -->
                </div>
                <hr>
                <div class="row col-md-12" style="max-width: 1140px;">
                    <div class=" seller-details">
                        <div class="float-left badge border-info pink round badge-border"><i class="font-medium-5 icon-line-height ft-user" style="font-size: 1.2rem;"></i></div>
                        <p style="margin-left: 46px;font-size: 13px;"> <span><b>John Mathew</b></span><br>
                        PRIVATE SELLER<br>
                        </p>                       
                               
                    </div>    
                     <div class="float-left badge danger secondary round badge-border"><i class="font-medium-5 icon-line-height ft-alert-triangle" style="font-size: 1.2rem;"></i></div> <a style="margin-top: 5px;"> Report  </a>          
                    <div  style="margin-top: 4px; margin-left: 70px;">
                        <div style="display: inline;width: 500px;margin-top: 10px;">
                        <button type="button" class="btn btn-success round  btn-min-width mr-1 mb-1 btn-sm " style="min-width: 4.5rem;"><i class="ft-message-square"></i>  Chat</button>
                        <button style="min-width: 4.5rem;" type="button" class="btn btn-success round  btn-min-width mr-1 mb-1 btn-sm"><i class="ft-phone"></i>  View Numer</button>
                        </div>
                    </div>
                </div>
                <hr>
                
			</div>
            
		</div>
		
<div class="clearfix" ></div>
		</div>
		<!-- <div class="modal-footer">
		<button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-info">Save</button>
		</div> -->
	</div>
</div>
</div>
	