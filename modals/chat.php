<div class="modal " id="chatModal" role="dialog">
<div class="modal-dialog modal-lg " role="document">
  <div class="modal-content border-info" style="height: 720px;">
    <div class="modal-header bg- white">
    <h4 class="modal-title white" id="myModalLabel11">Basic Modal</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    </div>
    <div class="modal-body">
      <div class="chat-application">
        <div class="app-content content">
        <div class="sidebar-left sidebar-fixed scroll" style="height: 643px; ">
            <div class="sidebar"><div class="sidebar-content card d-none d-lg-block">
                <div class="card-body chat-fixed-search">
                 
                  <h4 style="text-align: center;">Messages</h4>
              </div>
                <div id="users-list" class="list-group position-relative">
                    <div class="users-list-padding media-list">
                        <a href="#" class="media border-bottom-blue-grey border-bottom-lighten-5">
                            <div class="media-left pr-1">
                                <span class="avatar avatar-md"><img class="media-object rounded-circle" src="app-assets/images/portrait/small/avatar-s-3.png" alt="Generic placeholder image">                    
                                </span>
                            </div>
                            <div class="media-body w-100">
                                <h6 class="list-group-item-heading font-medium-1 text-bold-700">John &nbsp; <i class="ft-circle font-small-2 success"></i><span class="float-right primary"><span class="badge badge-pill badge-danger lighten-3">1</span></span></h6>
                                <p class="font-small-3 text-muted text-bold-500">3:55 PM</p>
                             
                            </div>
                        </a>
                        <a href="#" class="media border-bottom-blue-grey border-bottom-lighten-5 border-right-primary  border-right-2">
                            <div class="media-left pr-1">
                                <span class="avatar avatar-md"><img class="media-object rounded-circle box-shadow-3" src="app-assets/images/portrait/small/avatar-s-7.png" alt="Generic placeholder image">                    
                                </span>
                            </div>
                            <div class="media-body w-100">
                                <h6 class="list-group-item-heading font-medium-1 text-bold-700">Chris &nbsp; <i class="ft-circle font-small-2 success"></i><span class="float-right primary"><span class="badge badge-pill badge-danger lighten-3">3</span></span></h6>
                                <p class="font-small-3 text-muted text-bold-500">9:04 PM</p>
                              
                            </div>
                        </a>   
                        <a href="#" class="media border-bottom-blue-grey border-bottom-lighten-5">
                            <div class="media-left pr-1">
                                <span class="avatar avatar-md"><img class="media-object rounded-circle" src="app-assets/images/portrait/small/avatar-s-8.png" alt="Generic placeholder image">                    
                                </span>
                            </div>
                            <div class="media-body w-100">
                                <h6 class="list-group-item-heading font-medium-1 text-bold-700">Sara &nbsp; <i class="ft-circle font-small-2 danger"></i></h6>
                                <p class="font-small-3 text-muted text-bold-500">1:55 AM</p>
                               
                            </div>
                        </a>            
                        <a href="#" class="media border-bottom-blue-grey border-bottom-lighten-5">
                            <div class="media-left pr-1">
                                <span class="avatar avatar-md"><img class="media-object rounded-circle" src="app-assets/images/portrait/small/avatar-s-7.png" alt="Generic placeholder image">                    
                                </span>
                            </div>
                            <div class="media-body w-100">
                                <h6 class="list-group-item-heading font-medium-1 text-bold-700">Salim &nbsp; <i class="ft-circle font-small-2 warning"></i></h6>
                                <p class="font-small-3 text-muted text-bold-500">Today</p>
                              
                            </div>
                        </a>               
                        <a href="#" class="media border-bottom-blue-grey border-bottom-lighten-5">
                            <div class="media-left pr-1">
                                <span class="avatar avatar-md"><img class="media-object rounded-circle" src="app-assets/images/portrait/small/avatar-s-5.png" alt="Generic placeholder image">                    
                                </span>
                            </div>
                            <div class="media-body w-100">
                                <h6 class="list-group-item-heading font-medium-1 text-bold-700"> Mery&nbsp; <i class="ft-circle font-small-2 danger"></i></h6>
                                <p class="font-small-3 text-muted text-bold-500">Yesterday</p>
                               
                            </div>
                        </a> 
                        <a href="#" class="media border-bottom-blue-grey border-bottom-lighten-5">
                            <div class="media-left pr-1">
                                <span class="avatar avatar-md"><img class="media-object rounded-circle" src="app-assets/images/portrait/small/avatar-s-9.png" alt="Generic placeholder image">                    
                                </span>
                            </div>
                            <div class="media-body w-100">
                                <h6 class="list-group-item-heading font-medium-1 text-bold-700">John&nbsp; <i class="ft-circle font-small-2 danger"></i></h6>
                                <p class="font-small-3 text-muted text-bold-500">Friday</p>                                
                            </div>
                        </a>
                        <a href="#" class="media border-bottom-blue-grey border-bottom-lighten-5">
                            <div class="media-left pr-1">
                                <span class="avatar avatar-md"><img class="media-object rounded-circle" src="app-assets/images/portrait/small/avatar-s-7.png" alt="Generic placeholder image">                    
                                </span>
                            </div>
                            <div class="media-body w-100">
                                <h6 class="list-group-item-heading font-medium-1 text-bold-700">Nelson&nbsp; <i class="ft-circle font-small-2 success"></i></h6>
                                <p class="font-small-3 text-muted text-bold-500">Thursday</p>
                            </div>
                        </a>   
                        <a href="#" class="media border-bottom-blue-grey border-bottom-lighten-5">
                            <div class="media-left pr-1">
                                <span class="avatar avatar-md"><img class="media-object rounded-circle" src="app-assets/images/portrait/small/avatar-s-14.png" alt="Generic placeholder image">                    
                                </span>
                            </div>
                            <div class="media-body w-100">
                                <h6 class="list-group-item-heading font-medium-1 text-bold-700">Nelson&nbsp; <i class="ft-circle font-small-2 warning"></i></h6>
                                <p class="font-small-3 text-muted text-bold-500">Monday</p>
                            </div>
                        </a>        
                       
                        
                    </div>
                </div>
            </div>

        </div>
      </div>
      <div class="content-right scroll" style="height: 647px;">
        <div class="content-wrapper">
          <div class="content-wrapper-before"></div>
          <div class="content-header row">
          </div>
          <div class="content-body">
            <section class="chat-app-window">
                <div class="chats">
                  <div class="chats">
                    <div class="chat">
                      <div class="chat-avatar">
                        <a class="avatar" data-toggle="tooltip" href="#" data-placement="right" title="" data-original-title="">
                          <img src="app-assets/images/portrait/small/avatar-s-1.png" class="box-shadow-4" alt="avatar" />
                        </a>
                      </div>
                      <div class="chat-body">
                        <div class="chat-content">
                          <p>How are you Chris?</p>
                        </div>
                      </div>
                    </div>
                    <div class="chat chat-left">
                      <div class="chat-avatar">
                        <a class="avatar" data-toggle="tooltip" href="#" data-placement="left" title="" data-original-title="">
                          <img src="app-assets/images/portrait/small/avatar-s-7.png" class="box-shadow-4" alt="avatar" />
                        </a>
                      </div>
                      <div class="chat-body">
                        <div class="chat-content">
                          <p>Hey John, I am good.</p>
                          <p>Could you please send me more details?</p>
                        </div>
                        <div class="chat-content">
                          <p>It should be of smaller size.</p>
                        </div>
                      </div>
                    </div>
                    <div class="chat">
                      <div class="chat-avatar">
                        <a class="avatar" data-toggle="tooltip" href="#" data-placement="right" title="" data-original-title="">
                          <img src="app-assets/images/portrait/small/avatar-s-1.png" class="box-shadow-4" alt="avatar" />
                        </a>
                      </div>
                      <div class="chat-body">
                        <div class="chat-content">
                          <p>Here you go! Let me know if you need anything else.</p>
                        </div>
                      </div>
                    </div>
                    <div class="chat chat-left">
                      <div class="chat-avatar">
                        <a class="avatar" data-toggle="tooltip" href="#" data-placement="left" title="" data-original-title="">
                          <img src="app-assets/images/portrait/small/avatar-s-7.png" class="box-shadow-4" alt="avatar" />
                        </a>
                      </div>
                      <div class="chat-body">
                        <div class="chat-content">
                          <p>Looks cool!</p>
                        </div>
                        <div class="chat-content">
                          <p>It's perfect for me.</p>
                        </div>
                      </div>
                    </div>
                    <div class="chat">
                      <div class="chat-avatar">
                        <a class="avatar" data-toggle="tooltip" href="#" data-placement="right" title="" data-original-title="">
                          <img src="app-assets/images/portrait/small/avatar-s-1.png" class="box-shadow-4" alt="avatar" />
                        </a>
                      </div>
                      <div class="chat-body">
                        <div class="chat-content">
                          <p>Thanks, glad to help you.</p>
                        </div>
                      </div>
                    </div>
                    <div class="chat chat-left">
                      <div class="chat-avatar">
                        <a class="avatar" data-toggle="tooltip" href="#" data-placement="left" title="" data-original-title="">
                          <img src="app-assets/images/portrait/small/avatar-s-7.png" class="box-shadow-4" alt="avatar" />
                        </a>
                      </div>
                      <div class="chat-body">
                        <div class="chat-content">
                          <p>I will talk you later.</p>
                        </div>
                        <div class="chat-content">
                          <p>Bye.</p>
                        </div>
                      </div>
                    </div>
                    <div class="chat">
                      <div class="chat-avatar">
                        <a class="avatar" data-toggle="tooltip" href="#" data-placement="right" title="" data-original-title="">
                          <img src="app-assets/images/portrait/small/avatar-s-1.png" class="box-shadow-4" alt="avatar" />
                        </a>
                      </div>
                      <div class="chat-body">
                        <div class="chat-content">
                          <p>Sure, Feel free to get in touch.</p>
                        </div>
                        <div class="chat-content">
                          <p>Bye</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="chat-app-form" >
                <form class="chat-app-input d-flex">
                  <fieldset class="form-group position-relative has-icon-left col-8 m-0">
                    <div class="form-control-position">
                      <i class="icon-emoticon-smile"></i>
                    </div>
                    <input type="text" class="form-control" id="iconLeft4" placeholder="Type your message">
                    <div class="form-control-position control-position-right">
                    </div>
                  </fieldset>
                  <fieldset class="form-group position-relative has-icon-left col-4 m-0">
                    <button type="button" class="btn btn-danger">
                      <i class="la la-paper-plane-o d-xl-none"></i>
                      <span class="d-none d-lg-none d-xl-block">Send Message </span>
                    </button>
                  </fieldset>
                </form>
              </section>
          </div>
        </div>
      </div>
    </div>
           
    </div>
  </div>
    <!-- <div class="modal-footer">
    <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-info">Save</button>
    </div> -->
  </div>
</div>
</div>
  