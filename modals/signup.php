<div class="modal" id="signupModal" role="dialog">
<div class="modal-dialog modal-dialog-centered modal-m " role="document">
	<div class="modal-content border-info" >
		<div class="modal-header bg- white">
		<h4 class="modal-title white" id="myModalLabel11">Basic Modal</h4>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		</div>
		<div class="modal-body" >
		 <div class="col-12 d-flex align-items-center justify-content-center">
        <div class="col-md-12 col-10 box-shadow-2 p-0">
            <div class=" border-grey border-lighten-3 px-1 py-1 m-0">
                <div class="card-header border-0">
                    <div class="text-center mb-1">
                            <img src="assets/images/logo.png" alt="branding logo">
                    </div>
                    <div class="font-large-1  text-center">                       
                        Signup
                    </div>
                </div>
                <div class="card-content">
                   
                    <div class="card-body">
                        <form class="form-horizontal" action="index.html" novalidate>
                           <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" class="form-control round" id="user-name" placeholder="Name" required>
                                <div class="form-control-position">
                                    <i class="ft-user"></i>
                                </div>
                            </fieldset>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="email" class="form-control round" id="user-email" placeholder="Your Email Address" required>
                                <div class="form-control-position">
                                    <i class="ft-mail"></i>
                                </div>
                            </fieldset>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" class="form-control round" id="user-name" placeholder="Phone Number" required>
                                <div class="form-control-position">
                                    <i class="ft-phone"></i>
                                </div>
                            </fieldset>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="password" class="form-control round" id="user-password" placeholder="Enter Password" required>
                                <div class="form-control-position">
                                    <i class="ft-lock"></i>
                                </div>
                            </fieldset>
                             <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" class="form-control round" id="user-password" placeholder="Enter Locaton" required>
                                <div class="form-control-position">
                                    <i class="ft-map-pin"></i>
                                </div>
                                <div class="row skin-square skin float-right" style="margin-top: 10px;">
                                            <div class="col-md-12 col-sm-12">
                                                <fieldset>
                                                    <input type="checkbox" id="input-5">
                                                    <label for="input-5">Use Current Location</label>
                                                </fieldset>
                                            </div>
                                        </div>
                            </fieldset>
                            <img src="assets/images/google-captcha.jpg" style="width: 254px; margin-bottom: 10px;">
                            
                            
                            <div class="form-group text-center">
                                <button type="submit" class="btn round btn-block btn-glow btn-bg-gradient-x-purple-blue col-12 mr-1 mb-1">Register</button>
                            </div>
                           
                        </form>
                    </div>
                                                           
                </div>
            </div>
        </div>
    </div>
		</div>
		<!-- <div class="modal-footer">
		<button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-info">Save</button>
		</div> -->
	</div>
</div>
</div>
	