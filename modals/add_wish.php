<div class="modal " id="wishModal" role="dialog">
<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
	<div class="modal-content border-info">
		<div class="modal-header bg- white">
		<h4 class="modal-title white" id="myModalLabel11">Basic Modal</h4>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		</div>
		<div class="modal-body">
		<div class="row col-md-12">
			<div class="col-md-7 scrollable-container" style="margin-top: -43px;">  
            <div class="card-content collapse show">
                    <div class="card-body">
                        <form class="form">
                            <div class="form-body">
                                <h4 class="form-section">
                                    <i class="ft-flag"></i> Submit a Wish Item</h4>
                                <div class="form-group">
                                    <label for="companyName">Item Name <span class="danger"> *</span></label>
                                    <input type="text" id="item" class="form-control" placeholder="Item Name" name="item_name">
                                </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="companyName">Contact Number <span class="danger"> *</span></label>
                                            <input type="text" id="item" class="form-control" placeholder="Contact Number" name="company">
                                                <div class="row skin-square skin" style="margin-top: 10px;">
                                                    <div class="col-md-12 col-sm-12">
                                                        <fieldset>
                                                            <input type="checkbox" id="input-5">
                                                            <label for="input-5">Use Another Number</label>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="companyName">Email</label>
                                            <input type="text" id="item" class="form-control" placeholder="Email" name="company">
                                             <div class="row skin-square skin" style="margin-top: 10px;">
                                                    <div class="col-md-12 col-sm-12">
                                                        <fieldset>
                                                            <input type="checkbox" id="input-5">
                                                            <label for="input-5">Use Another Email</label>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="form-group">
                                        <label for="companyName">Location <span class="danger"> *</span></label>
                                        <input type="text" id="item" class="form-control" placeholder="Type Here" name="item_name">
                                        <div class="row skin-square skin" style="margin-top: 10px;">
                                            <div class="col-md-12 col-sm-12">
                                                <fieldset>
                                                    <input type="checkbox" id="input-5">
                                                    <label for="input-5">Use Another Location</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="companyName">Quantity</label>                                             
                                                <input type="text" class="form-control" placeholder="Quantity" name="rateperhour">
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="companyinput1">Budget</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="la la-rupee" style="font-size: 1.2rem;"></i></span>
                                                </div>
                                                 <input type="text" class="form-control" placeholder="Rate" name="rateperhour">
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>     
                            </div>
                             <div class="form-actions">
                                <button type="button" class="btn btn-danger mr-1">
                                    <i class="ft-x"></i> Cancel
                                </button>
                                <button type="submit" class="btn btn-primary">
                                    <i class="la la-check-square-o"></i> Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>              
				
			</div>
			<div class="col-md-5">
                <h6 class="card-title">Sample Image </h6>
                 <div class="images">
                    <div class="pic">
                      add
                    </div>
                  </div>
                  <br>
                  <p class="card-text"><code>Ads with 1-4 photos get 5x more responses!</code> and <code>Accepted formats are .jpg, .gif & .png. Size < 5MB.</code> </p>
                   <div class="row skin-square skin" style="margin-top: 10px;">
                        <div class="col-md-12 col-sm-12">
                            <fieldset>
                                <input type="checkbox" id="input-5">
                                <label for="input-5">Choose Image From Our Gallery</label>
                                <button style="min-width: 4.5rem;" type="button" class="btn btn-success round  btn-min-width mr-1 mb-1 btn-sm"><i class="ft-upload"></i> Upload</button>
                            </fieldset>
                        </div>
                    </div>                

                 
				 <!-- <div class="card">
                    <div class="card-header">
                        <h6 class="card-title">Image Upoload</h6>
                    </div>
                    <div class="card-block">
                        <div class="card-body">
                            <fieldset class="form-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile01">
                                    <label class="custom-file-label" for="inputGroupFile01">Choose Main Image</label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
				 <div class="card-content collapse show">
                    <div class="card-body">
                        <p class="card-text"><code>Ads with 1-4 photos get 5x more responses!</code> and <code>Accepted formats are .jpg, .gif & .png. Size < 5MB.</code> </p>
                        <form action="#" class="dropzone dropzone-area" id="dpz-remove-thumb">
                            <div class="dz-message" style="font-size: 1.5em;">Drop Images Here Or Click To Upload</div>
                        </form>
                    </div>
                </div> -->
              
                
			</div>
            
		</div>
		<div class="clearfix"></div>
		</div>	</div>
</div>
</div>
	